kwcoco.examples package
=======================

Submodules
----------

.. toctree::
   :maxdepth: 4

   kwcoco.examples.bench_large_hyperspectral
   kwcoco.examples.draw_gt_and_predicted_boxes
   kwcoco.examples.faq
   kwcoco.examples.getting_started_existing_dataset
   kwcoco.examples.loading_multispectral_data
   kwcoco.examples.modification_example
   kwcoco.examples.simple_kwcoco_torch_dataset
   kwcoco.examples.vectorized_interface

Module contents
---------------

.. automodule:: kwcoco.examples
   :members:
   :undoc-members:
   :show-inheritance:
